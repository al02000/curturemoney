package com.sjw.curturemoney.common;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.sjw.curturemoney.dto.FirebasePost;
import com.sjw.curturemoney.dto.PurcharseDto;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Common {

    public static final int PURCHASE = 2000;

    public static void postFirebaseDatabase(boolean add, String id, int point){
        DatabaseReference mPostReference = FirebaseDatabase.getInstance().getReference();
        Map<String, Object> childUpdates = new HashMap<>();
        Map<String, Object> postValues = null;

        if(add){
            FirebasePost post = new FirebasePost(id, point);
            postValues = post.toMap();
        }
        childUpdates.put("/users/"+id.replace("/", ""), postValues);
        mPostReference.updateChildren(childUpdates);
    }

    public static void setPurchaseMoney(boolean add, String phoneNum, int point){
        DatabaseReference mPostReference = FirebaseDatabase.getInstance().getReference();
        Map<String, Object> childUpdates = new HashMap<>();
        Map<String, Object> postValues = null;
        long now = System.currentTimeMillis();
        Date mDate = new Date(now);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd:HH:mm:ss");
        String key = sdf.format(mDate);
        if(add){
            PurcharseDto dto = new PurcharseDto(phoneNum, point);
            postValues = dto.toMap();
        }
        childUpdates.put("/purchase/"+key, postValues);
        mPostReference.updateChildren(childUpdates);
    }

}
