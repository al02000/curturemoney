package com.sjw.curturemoney.dto;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class FirebasePost {
    public String id;
    public int point;

    public FirebasePost(){

    }

    public FirebasePost(String mId, int mPoint){
        this.id = mId;
        this.point = mPoint;
    }

    @Exclude
    public Map<String, Object> toMap(){
        HashMap<String, Object> result = new HashMap<>();
        result.put("id", id);
        result.put("point", point);

        return result;
    }
}
