package com.sjw.curturemoney.dto;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

public class PurcharseDto {

    private String phoneNum;
    private int price;

    public PurcharseDto(){

    }

    public PurcharseDto(String phone, int price){
        this.phoneNum = phone;
        this.price = price;
    }

    @Exclude
    public Map<String,  Object> toMap(){
        HashMap<String, Object> result = new HashMap<>();
        result.put("phone", phoneNum);
        result.put("point", price);

        return result;
    }
}
