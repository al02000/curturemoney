package com.sjw.curturemoney.activity;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.TextView;


import com.fpang.lib.FpangSession;
import com.fpang.lib.SessionCallback;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.igaworks.IgawCommon;
import com.igaworks.adpopcorn.IgawAdpopcornExtension;
import com.igaworks.adpopcorn.cores.model.APClientRewardItem;
import com.igaworks.adpopcorn.interfaces.IAPClientRewardCallbackListener;
import com.scottyab.aescrypt.AESCrypt;
import com.sjw.curturemoney.adapter.MyPagerAdapter;
import com.sjw.curturemoney.R;
import com.sjw.curturemoney.common.Common;
import com.sjw.curturemoney.dto.FirebasePost;
import com.sjw.curturemoney.fragment.FragmentOne;
import com.sjw.curturemoney.fragment.FragmentTwo;
import com.tnkfactory.ad.BannerAdView;
import com.tnkfactory.ad.ServiceCallback;
import com.tnkfactory.ad.TnkAdListener;
import com.tnkfactory.ad.TnkSession;

import java.security.GeneralSecurityException;

public class MainActivity extends AppCompatActivity {

    private final int PHONE_STATE_PERMISSION = 1000;
    private int userPoint = 0;
    private TextView tvSaveGold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        userPoint = getUserGold();
        checkPermission();
        setLayout();

        IgawAdpopcornExtension.setClientRewardCallbackListener(this, new IAPClientRewardCallbackListener(){
            int point = 0;
            @Override
            public void onGetRewardInfo(boolean isSuccess, String resultMsg, APClientRewardItem[] rewardItems) {
                for(APClientRewardItem rewardItem : rewardItems){
                    Log.i("TEST", "CampaignKey => "+rewardItem.getCampaignKey());
                    Log.i("TEST", "CampaignTitle"+rewardItem.getCampaignTitle());
                    Log.i("TEST", "Gold => "+rewardItem.getRewardQuantity());
                    point = (int)rewardItem.getRewardQuantity();
                    if(point > 0){
                        rewardItem.didGiveRewardItem();
                    }
                }
            }

            @Override
            public void onDidGiveRewardItemResult(boolean isSuccess, String resultMsg, int resultCode, String completedRewardKey) {
                if(isSuccess){
                    userPoint = userPoint+point;
                    Common.postFirebaseDatabase(true, TnkSession.getUserName(MainActivity.this), userPoint);
                }
                Log.i("TEST", "isSuccess => "+isSuccess);
                Log.i("TEST", "resultMsg => "+resultMsg);
                Log.i("TEST", "resultCode => "+resultCode);
                Log.i("TEST", "completeKey => "+completedRewardKey);
            }
        });
    }

    private void setLayout(){
        tvSaveGold = (TextView)findViewById(R.id.tv_main_saveGold);
        tvSaveGold.setText("적립골드 : "+getUserGold()+"Gold");

        ViewPager viewPager = (ViewPager)findViewById(R.id.viewpager);
        if(viewPager != null){
            setupViewPager(viewPager);
        }
        TabLayout tabLayout = (TabLayout)findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        BannerAdView bannerAdView = (BannerAdView)findViewById(R.id.banner);
        bannerAdView.loadAd(TnkSession.CPC);
    }

    private void setupViewPager(ViewPager viewPager){
        MyPagerAdapter adapter = new MyPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new FragmentOne(), "충전하기");
        adapter.addFragment(new FragmentTwo(), "구매하기");
        viewPager.setAdapter(adapter);
    }

    private void checkPermission(){
        FpangSession.init(this);
        IgawCommon.startSession(this);
        IgawAdpopcornExtension.setCashRewardAppFlag(this, true);
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, PHONE_STATE_PERMISSION);
        }else{
            TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
            try{
                String encrypt = AESCrypt.encrypt("device", tm.getDeviceId());
                encrypt = encrypt.replace("/","");
                TnkSession.setUserName(this, encrypt);
                FpangSession.setUserId(encrypt);
                IgawCommon.setUserId(this, encrypt);
                setDataChangeListener();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @Nullable String[] permissions, @Nullable int[] grantResult){
        switch (requestCode){
            case PHONE_STATE_PERMISSION:
                if(grantResult.length > 0 && grantResult[0] != PackageManager.PERMISSION_GRANTED){
                    MainActivity.this.finish();
                }else{
                    try{
                        TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
                        String encrypt = AESCrypt.encrypt("device", tm.getDeviceId());
                        encrypt = encrypt.replace("/", "");
                        TnkSession.setUserName(this, encrypt);
                        FpangSession.setUserId(encrypt);
                        IgawCommon.setUserId(this, encrypt);
                        setDataChangeListener();
                    }catch (SecurityException e){
                        e.printStackTrace();
                    }catch (GeneralSecurityException e){
                        e.printStackTrace();
                    }
                }
                break;
        }
    }

    @Override
    protected void onResume(){
        super.onResume();

        TnkSession.withdrawPoints(this, "골드적립", false, new ServiceCallback() {
            @Override
            public void onReturn(Context context, Object o) {
                int point = (int)o;
                if(point > 0){
                    userPoint = userPoint+point;
                    Common.postFirebaseDatabase(true, TnkSession.getUserName(MainActivity.this), userPoint);
                }
            }
        });

        FpangSession.withdrawUserPoint(this, new SessionCallback() {
            @Override
            public void onResult(Context context, Object o) {
                int point = (int)o;
                if(point > 0){
                    userPoint = userPoint+point;
                    Common.postFirebaseDatabase(true, TnkSession.getUserName(MainActivity.this), userPoint);
                }
            }
        }, "골드적립");

        tvSaveGold.setText("적립골드 : "+getUserGold()+"Gold");
    }

    @Override
    protected void onPause(){
        super.onPause();
        IgawCommon.endSession();
    }

    private void setDataChangeListener(){
        Common.postFirebaseDatabase(true, TnkSession.getUserName(MainActivity.this), userPoint);
        Query query = FirebaseDatabase.getInstance().getReference().child("users").child(TnkSession.getUserName(MainActivity.this));
        query.addValueEventListener(mValueEvent);
    }

    ValueEventListener mValueEvent = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            Log.e("TEST", "getChildren => "+dataSnapshot.getValue());
            FirebasePost get = dataSnapshot.getValue(FirebasePost.class);
            String[] info = {get.id, String.valueOf(get.point)};
            userPoint = Integer.parseInt(info[1]);
            saveUserGold(userPoint);
            tvSaveGold.setText("적립골드 : "+getUserGold()+"Gold");
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    public void saveUserGold(int gold){
        SharedPreferences pref = getSharedPreferences("gold", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt("gold", gold);
        editor.commit();
    }

    public int getUserGold(){
        int gold = 0;
        SharedPreferences pref = getSharedPreferences("gold", MODE_PRIVATE);
        gold = pref.getInt("gold", 0);
        return gold;
    }

    @Override
    public void onBackPressed(){
        TnkSession.prepareInterstitialAd(this, TnkSession.CPC);
        TnkSession.showInterstitialAd(this, new TnkAdListener() {
            @Override
            public void onClose(int i) {
                if(i == TnkAdListener.CLOSE_EXIT)
                    MainActivity.this.finish();
            }

            @Override
            public void onShow() {

            }

            @Override
            public void onFailure(int i) {
                MainActivity.this.finish();
            }

            @Override
            public void onLoad() {

            }
        });

    }
}
