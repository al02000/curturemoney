package com.sjw.curturemoney.activity;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sjw.curturemoney.R;
import com.sjw.curturemoney.common.Common;
import com.tnkfactory.ad.TnkSession;

public class ActPurchase extends Activity {

    @Override
    protected void onCreate(Bundle saved){
        super.onCreate(saved);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.act_purchase);
        this.setFinishOnTouchOutside(false);
        final int point = getIntent().getIntExtra("point", 0);
        Log.i("TEST ", "PurchasePoint => "+point);
        final EditText etPhone = (EditText)findViewById(R.id.et_purchase_phone);
        Button btnConfirm = (Button)findViewById(R.id.btn_purchase_confirm);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etPhone.getText().length() == 0 ){
                    Toast.makeText(ActPurchase.this, "연락처를 입력하세요", Toast.LENGTH_SHORT).show();
                }else{
                    Common.setPurchaseMoney(true, etPhone.getText().toString(), point);
                    Common.postFirebaseDatabase(true, TnkSession.getUserName(ActPurchase.this),getUserGold()-point);
                    Toast.makeText(ActPurchase.this, "정상적으로 접수되었습니다.", Toast.LENGTH_LONG).show();
                    finish();
                }
            }
        });
    }

    public void saveUserGold(int gold){
        SharedPreferences pref = getSharedPreferences("gold", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt("gold", gold);
        editor.commit();
    }

    public int getUserGold(){
        int gold = 0;
        SharedPreferences pref = getSharedPreferences("gold", Context.MODE_PRIVATE);
        gold = pref.getInt("gold", 0);
        return gold;
    }
}
