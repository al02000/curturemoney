package com.sjw.curturemoney.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.sjw.curturemoney.R;
import com.sjw.curturemoney.activity.ActPurchase;
import com.sjw.curturemoney.common.Common;

public class FragmentTwo extends Fragment implements OnClickListener{

    private LinearLayout llItem1;
    private LinearLayout llItem2;
    private LinearLayout llItem3;
    private LinearLayout llItem4;
    private LinearLayout llItem5;
    private LinearLayout llItem6;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View v = inflater.inflate(R.layout.frag_two, container, false);
        setLayout(v);
        return v;
    }

    private void setLayout(View v){
        llItem1 = (LinearLayout)v.findViewById(R.id.ll_frag_two_item1);
        llItem2 = (LinearLayout)v.findViewById(R.id.ll_frag_two_item2);
        llItem3 = (LinearLayout)v.findViewById(R.id.ll_frag_two_item3);
        llItem4 = (LinearLayout)v.findViewById(R.id.ll_frag_two_item4);
        llItem5 = (LinearLayout)v.findViewById(R.id.ll_frag_two_item5);
        llItem6 = (LinearLayout)v.findViewById(R.id.ll_frag_two_item6);

        llItem1.setOnClickListener(this);
        llItem2.setOnClickListener(this);
        llItem3.setOnClickListener(this);
        llItem4.setOnClickListener(this);
        llItem5.setOnClickListener(this);
        llItem6.setOnClickListener(this);
    }

    public void saveUserGold(int gold){
        SharedPreferences pref = getActivity().getSharedPreferences("gold", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt("gold", gold);
        editor.commit();
    }

    public int getUserGold(){
        int gold = 0;
        SharedPreferences pref = getActivity().getSharedPreferences("gold", Context.MODE_PRIVATE);
        gold = pref.getInt("gold", 0);
        return gold;
    }

    @Override
    public void onClick(View v) {
        Intent cIntent = new Intent(getActivity(), ActPurchase.class);
        switch (v.getId()){
            case R.id.ll_frag_two_item1:
                if(getUserGold() > 1000){
                    cIntent.putExtra("point", 1000);
                    getActivity().startActivity(cIntent);
                }else{
                    Toast.makeText(getActivity(), "골드가 부족합니다.", Toast.LENGTH_SHORT).show();
                    return;
                }
                break;

            case R.id.ll_frag_two_item2:
                if(getUserGold() > 3000){
                    cIntent.putExtra("point", 3000);
                    getActivity().startActivity(cIntent);
                }else{
                    Toast.makeText(getActivity(), "골드가 부족합니다.", Toast.LENGTH_SHORT).show();
                    return;
                }
                break;

            case R.id.ll_frag_two_item3:
                if(getUserGold() > 5000){
                    cIntent.putExtra("point", 5000);
                    getActivity().startActivity(cIntent);
                }else{
                    Toast.makeText(getActivity(), "골드가 부족합니다.", Toast.LENGTH_SHORT).show();
                    return;
                }
                break;

            case R.id.ll_frag_two_item4:
                if(getUserGold() > 10000){
                    cIntent.putExtra("point", 10000);
                    getActivity().startActivity(cIntent);
                }else{
                    Toast.makeText(getActivity(), "골드가 부족합니다.", Toast.LENGTH_SHORT).show();
                    return;
                }
                break;

            case R.id.ll_frag_two_item5:
                if(getUserGold() > 30000){
                    cIntent.putExtra("point", 30000);
                    getActivity().startActivity(cIntent);
                }else{
                    Toast.makeText(getActivity(), "골드가 부족합니다.", Toast.LENGTH_SHORT).show();
                    return;
                }

                break;

            case R.id.ll_frag_two_item6:
                if(getUserGold() > 50000){
                    cIntent.putExtra("point", 50000);
                    getActivity().startActivity(cIntent);
                }else{
                    Toast.makeText(getActivity(), "골드가 부족합니다.", Toast.LENGTH_SHORT).show();
                    return;
                }
                break;
        }
    }
}
