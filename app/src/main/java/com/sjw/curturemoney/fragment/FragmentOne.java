package com.sjw.curturemoney.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.fpang.lib.FpangSession;
import com.igaworks.adpopcorn.IgawAdpopcorn;


import com.sjw.curturemoney.R;
import com.tnkfactory.ad.TnkSession;

public class FragmentOne extends Fragment{

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View v = (View)inflater.inflate(R.layout.frag_one, container, false);
        setLayout(v);
        return v;
    }

    private void setLayout(View v){
        LinearLayout llTnk = (LinearLayout)v.findViewById(R.id.ll_frag_one_tnk);
        LinearLayout llAdSync = (LinearLayout)v.findViewById(R.id.ll_frag_one_adsync);
        LinearLayout llAdPopcorn = (LinearLayout)v.findViewById(R.id.ll_frag_one_adPopcorn);

        llTnk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTnk();
            }
        });

        llAdSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAdSync();
            }
        });

        llAdPopcorn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAdPopcorn();
            }
        });
    }

    private void showTnk(){
        TnkSession.showAdList(getActivity(), "첫번째 충전소");
    }

    private void showAdSync(){
        FpangSession.showAdsyncList(getActivity(), "두번째 충전소");
    }

    private void showAdPopcorn(){
        IgawAdpopcorn.openOfferWall(getActivity());
    }
}
